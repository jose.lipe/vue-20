import TimeListComponent from './time-list.component';
import TimeJogoComponent from './time-jogo.component';
import TimeZonaComponent from './time-zona.component';
//import event from '../event';
import store from '../store';

export default {
    components: {
        'time-list': TimeListComponent,
        'time-jogo': TimeJogoComponent,
        'time-zona': TimeZonaComponent
    },
    template: `
        <div class="container">
          <div class="row">
            <h1>Campeonato Brasileiro - Série A</h1>
            <a class="btn btn-primary" @click.prevent="showTabela">Ver Tabela</a>
            <a class="btn btn-primary" @click.prevent="showNovoJogo">Novo Jogo</a>
            <a class="btn btn-primary" @click.prevent="showZona">Ver Zonas</a>
            <br><br>
            <div v-if="view == 'tabela'">
                  <time-list></time-list>
            </div>
            <div v-if="view == 'novojogo'">
                  <time-jogo></time-jogo>
            </div>
            <div v-if="view == 'zona'">
                  <time-zona></time-zona>
            </div>
          </div>
        </div>
    `,
    /*mounted() {
        event.$on('show-time-list', () => {
              this.view = 'tabela';
        });

        event.$on('show-time-jogo', () => {
              this.view = 'novojogo';
        });
    },*/
    computed: {
        view() {
            return store.state.view;
            //return this.$store.state.view;
        }
    },
    methods: {
        /*showView(view) {
            this.view = view;
        },*/
        showNovoJogo() {
            //event.$emit('show-time-jogo');
            //event.$emit('get-times', this.times);
            store.commit('show-time-novojogo');
        },
        showZona() {
            store.commit('show-time-zona');
        },
        showTabela() {
            store.commit('show-time-list');
        }
    }
};
